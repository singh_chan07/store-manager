Store Manager
================
Store Manager is basically a demo application built using Spring boot, Spring Data JPA, 
and RESTful Web-services. It also uses a feign client to consume other
REST service. 

Swagger(OpenAPI 3.0): http://localhost:8090/swagger-ui-custom.html

Commits
-------------------------------
*Following changes has been committed so far:*

1. Commits v2

    1. Added a base exception which all other exception extends and 
       will throw InternalServerError(500).
    2. Added a base Dto.
    3. Created a base repository which all other repositories will extend.
    4. Use of Feign client to consume services from another API.
    5. Created two Spring profile for *local* and *dev* with relevant config files.
    6. added spring security support using jwt.
    7. Use of configuration properties to externalize configs and to read message properties.
    8. Use of application.yml instead of  application.properties.
    9. Implemented OpenAPI 3(Swagger)
    10. Added logging using @Slf4j
    11. Created unit tests for services and controllers.
   

2. Commits v1

    1. Changed APIs according to convention/best practices.
    2. Created a base entity for all entities
    3. Exception Handled at Service layer
    4. Use of Model mapper to convert between entities and dtos.
    5. Use of @Getter, @Setter and @..Constructor at class level.
    6. Custom Exception Handler for validation & Other exceptions
    7. Added Internationalization Support to the application
    8. Externalized Validation and Error messages.



APIs:
--------
1. uri */stores*, method=*GET* : fetches all stores.
2. uri */stores*, method=*POST* : Adds a store.
1. uri */stores/{storeId}*, method=*GET* : fetches the store with the *storeId*.
3. uri */stores/{storeId}*, method=*PUT* : Updates the store with the *storeId*.
4. uri */stores/{storeId}*, method=*DELETE* : Deletes the store with the *storeId*.
5. uri */stores/{storeId}/brands*, method=*GET* : Fetch all brands of the store with the id of *storeId*.
6. uri */stores/{storeId}/brands*, method=*POST* : Adds a brand to the store with the id of *storeId*.
5. uri */stores/{storeId}/brands/{brandId}*, method=*GET* : Fetch the brand with *brandId* of the store with the id of *storeId*.
7. uri */stores/{storeId}/brands/{brandId}*, method=*PUT* : Updates a brand with *brandId* of the store with the id of *storeId*.
8. uri */stores/{storeId}/brands/{brandId}*, method=*DELETE* : Deletes a brand with the *brandId* of the store with the id of *storeId*.