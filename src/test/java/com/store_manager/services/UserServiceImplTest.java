package com.store_manager.services;

import com.store_manager.dtos.StoreDto;
import com.store_manager.entities.Role;
import com.store_manager.entities.Store;
import com.store_manager.entities.User;
import com.store_manager.exceptions.ResourceNotFoundException;
import com.store_manager.exceptions.UserAlreadyExist;
import com.store_manager.exceptions.UserNotFound;
import com.store_manager.repositories.StoreRepository;
import com.store_manager.repositories.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.*;

import static org.junit.Assert.*;

import static org.junit.Assert.assertNotNull;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserServiceImplTest {

    @Autowired
    UserService userService;

    @MockBean
    UserRepository userRepository;

    @Autowired
    MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;


    @Before
    public void setup(){
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    @Test
    public void addSuccessful() {
        User user = new User();
        user.setUserName("username");
        user.setPassword("password");
        user.setRoles(Set.of(
                new Role("user", null),
                new Role("admin", null)
        ));


        Mockito.when(userRepository.findByUserName(ArgumentMatchers.anyString())).thenReturn(Optional.empty());
        Mockito.when(userRepository.save(ArgumentMatchers.any())).thenReturn(user);

        assertEquals(user.getUsername(), userService.add(user).getUsername());
        verify(userRepository, times(1)).findByUserName(ArgumentMatchers.anyString());
        verify(userRepository, times(1)).save(ArgumentMatchers.any());
        verifyNoMoreInteractions(userRepository);

    }

    @Test
    public void addThrowUserExistException() {

        User user = new User();
        user.setUserName("username");
        user.setPassword("password");
        user.setRoles(Set.of(
                new Role("user", null),
                new Role("admin", null)
        ));

        Mockito.when(userRepository.findByUserName(ArgumentMatchers.anyString()))
                .thenThrow(new UserAlreadyExist("UserExist"));

        String actual = Assertions.assertThrows(UserAlreadyExist.class, ()->{
            userService.add(user);
        }).getMessage();
        String expected = "UserExist";
        assertEquals(expected, actual);
        verify(userRepository, times(1)).findByUserName(ArgumentMatchers.anyString());

        verifyNoMoreInteractions(userRepository);

    }

    @Test
    public void getUserSuccessful() {
        User user = new User();
        user.setUserName("username");
        user.setPassword("password");
        user.setRoles(Set.of(
                new Role("user", null),
                new Role("admin", null)
        ));


        Mockito.when(userRepository.findByUserName(ArgumentMatchers.anyString())).thenReturn(Optional.of(user));

        assertEquals(user.getUsername(), userService.getUser("username").getUsername());
        verify(userRepository, times(1)).findByUserName(ArgumentMatchers.anyString());
        verifyNoMoreInteractions(userRepository);
    }


    @Test
    public void getThrowUserNotFoundException() {

        User user = new User();
        user.setUserName("username");
        user.setPassword("password");
        user.setRoles(Set.of(
                new Role("user", null),
                new Role("admin", null)
        ));
        Mockito.when(userRepository.findByUserName(ArgumentMatchers.anyString()))
                .thenThrow(new UserNotFound("UserNotExist"));

        String actual = Assertions.assertThrows(UserNotFound.class, ()->{
            userService.getUser("username");
        }).getMessage();
        String expected = "UserNotExist";
        assertEquals(expected, actual);
        verify(userRepository, times(1)).findByUserName(ArgumentMatchers.anyString());

        verifyNoMoreInteractions(userRepository);

    }


    @Test
    public void deleteThrowUserNotFoundException() {

        User user = new User();
        user.setUserName("username");
        user.setPassword("password");
        user.setRoles(Set.of(
                new Role("user", null),
                new Role("admin", null)
        ));
        Mockito.when(userRepository.findByUserName(ArgumentMatchers.anyString()))
                .thenThrow(new UserNotFound("UserNotExist"));

        String actual = Assertions.assertThrows(UserNotFound.class, ()->{
            userService.deleteUser("username");
        }).getMessage();
        String expected = "UserNotExist";
        assertEquals(expected, actual);
        verify(userRepository, times(1)).findByUserName(ArgumentMatchers.anyString());

        verifyNoMoreInteractions(userRepository);

    }

    @Test
    public void deleteUserSuccessful() {
        User user = new User();
        user.setUserName("username");
        user.setPassword("password");
        user.setRoles(Set.of(
                new Role("user", null),
                new Role("admin", null)
        ));


        Mockito.when(userRepository.findByUserName(ArgumentMatchers.anyString())).thenReturn(Optional.of(user));
        Mockito.when(userRepository.save(ArgumentMatchers.any())).thenReturn(user);
        userService.deleteUser("username");
        verify(userRepository, times(1)).findByUserName(ArgumentMatchers.anyString());
        verify(userRepository, times(1)).save(ArgumentMatchers.any());
        verifyNoMoreInteractions(userRepository);
    }

    @Test
    public void getRoles() {

        User user = new User();
        user.setUserName("username");
        user.setPassword("password");
        user.setRoles(Set.of(
                new Role("user", null),
                new Role("admin", null)
        ));
        Mockito.when(userRepository.findByUserName(ArgumentMatchers.anyString())).thenReturn(Optional.of(user));
        assertEquals(user.getRoles(), userService.getRoles("username"));
    }
}