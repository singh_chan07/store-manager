package com.store_manager.services;

import com.store_manager.dtos.BrandDto;
import com.store_manager.dtos.StoreDto;
import com.store_manager.entities.Brand;
import com.store_manager.entities.Store;
import com.store_manager.exceptions.ResourceAlreadyExistsException;
import com.store_manager.exceptions.ResourceNotFoundException;
import com.store_manager.repositories.BrandRepository;
import com.store_manager.repositories.StoreRepository;
import com.store_manager.utils.Status;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class BrandServiceImplTest {

    @MockBean
    BrandRepository brandRepository;

    @MockBean
    StoreRepository storeRepository;

    @Autowired
    BrandService brandService;

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    Map<Long, BrandDto> repo = new HashMap<>();

    @Before
    public void setup(){
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context).build();


        repo.put(1l, new BrandDto("brand1", "b1"));
        repo.put(2l, new BrandDto("brand2", "b2"));
        repo.put(3l, new BrandDto("brand3", "b3"));

    }



    @Test
    public void getBrandByIdFound() {

        Brand brand =  new Brand();
        brand.setName("brand1");
        brand.setInitials("b1");
        Mockito.when(brandRepository.findByIdAndStatus(ArgumentMatchers.anyLong(), ArgumentMatchers.any()))
                .thenReturn((Optional.of(brand)));

        assertEquals(brandService.get(1l).getName(), brand.getName());
        assertEquals(brandService.get(1l).getInitials(), brand.getInitials());
        verify(brandRepository, times(2)).findByIdAndStatus(1l, Status.ACTIVE);
        verifyNoMoreInteractions(brandRepository);
    }

    @Test
    public void getBrandByIdNotFoundException(){
        Mockito.when(brandRepository.findByIdAndStatus(1l, Status.ACTIVE))
                .thenThrow(new ResourceNotFoundException("NoResource"));
        String actual = Assertions.assertThrows(ResourceNotFoundException.class, ()->{
            brandService.get(1l);
        }).getMessage();
        String expected = "NoResource";
        assertEquals(expected, actual);
        verify(brandRepository, times(1)).findByIdAndStatus(1l, Status.ACTIVE);
        verifyNoMoreInteractions(brandRepository);

    }

    @Test
    public void updateBrandSuccessful(){
        Brand brand =  new Brand();
        brand.setName("brand1");
        brand.setInitials("b1");
        Mockito.when(brandRepository.findByIdAndStatus(ArgumentMatchers.anyLong(), ArgumentMatchers.any()))
                .thenReturn((Optional.of(brand)));
        Mockito.when(brandRepository.save(ArgumentMatchers.any()))
                .thenReturn(brand);

        BrandDto brandResult = brandService.update(1l, modelMapper.map(brand, BrandDto.class));
        assertEquals(brandResult.getName(), brand.getName());
        assertEquals(brandResult.getInitials(), brand.getInitials());

        verify(brandRepository, times(1)).findByIdAndStatus(1l, Status.ACTIVE);
        verify(brandRepository, times(1)).save(brand);
        verifyNoMoreInteractions(brandRepository);

    }

    @Test
    public void updateThrowsResourceNotFoundException(){

        Brand brand =  new Brand();
        brand.setName("brand1");
        brand.setInitials("b1");
        Mockito.when(brandRepository.findByIdAndStatus(1l, Status.ACTIVE))
                .thenThrow(new ResourceNotFoundException("NoResource"));
        String actual = Assertions.assertThrows(ResourceNotFoundException.class, ()->{
            brandService.update(1l, modelMapper.map(brand, BrandDto.class));
        }).getMessage();
        String expected = "NoResource";
        assertEquals(expected, actual);
        verify(brandRepository, times(1)).findByIdAndStatus(1l, Status.ACTIVE);
        verifyNoMoreInteractions(brandRepository);

    }

    @Test
    public void deleteSuccessful() {

        Brand brand =  new Brand();
        brand.setName("brand1");
        brand.setInitials("b1");
        Mockito.when(brandRepository.findByIdAndStatus(ArgumentMatchers.anyLong(), ArgumentMatchers.any()))
                .thenReturn((Optional.of(brand)));
        Mockito.when(brandRepository.save(ArgumentMatchers.any()))
                .thenReturn(brand);

        BrandDto brandDto = brandService.delete(1l);

        assertEquals(brandDto.getName(), brand.getName());
        assertEquals(brandDto.getInitials(), brand.getInitials());

        verify(brandRepository, times(1)).findByIdAndStatus(1l, Status.ACTIVE);
        verify(brandRepository, times(1)).save(ArgumentMatchers.any());
        verifyNoMoreInteractions(brandRepository);
    }

    @Test
    public void deleteThrowsResourceNotFoundException(){

        Brand brand =  new Brand();
        brand.setName("brand1");
        brand.setInitials("b1");
        Mockito.when(brandRepository.findByIdAndStatus(1l, Status.ACTIVE))
                .thenThrow(new ResourceNotFoundException("NoResource"));
        String actual = Assertions.assertThrows(ResourceNotFoundException.class, ()->{
            brandService.delete(1l);
        }).getMessage();
        String expected = "NoResource";
        assertEquals(expected, actual);
        verify(brandRepository, times(1)).findByIdAndStatus(1l, Status.ACTIVE);
        verifyNoMoreInteractions(brandRepository);

    }

    @Test
    public void getAllSuccessful(){
        Store store = new Store();
        store.setName("store");

        Brand brand1 =  new Brand();
        brand1.setName("brand1");
        brand1.setInitials("b1");

        Brand brand2 =  new Brand();
        brand2.setName("brand2");
        brand2.setInitials("b2");

        store.setBrandList(List.of(brand1, brand2));

        Mockito.when(storeRepository.findByIdAndStatus(ArgumentMatchers.anyLong(), ArgumentMatchers.any()))
                .thenReturn(Optional.of(store));
        List<BrandDto> brandDtoList = brandService.getAllByStore(1);

        assertEquals(brandDtoList.size(), store.getBrandList().size());
        verify(storeRepository, times(1))
                .findByIdAndStatus(ArgumentMatchers.anyLong(), ArgumentMatchers.any());
    }


}