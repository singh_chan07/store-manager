package com.store_manager.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.store_manager.dtos.StoreDto;
import com.store_manager.entities.Store;
import com.store_manager.exceptions.ResourceAlreadyExistsException;
import com.store_manager.exceptions.ResourceNotFoundException;
import com.store_manager.repositories.StoreRepository;
import com.store_manager.utils.Status;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class StoreServiceImplTest {


    @MockBean
    StoreRepository storeRepository;

    @Autowired
    StoreService storeService;


    ModelMapper modelMapper;

    @Autowired
    MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    Map<Long, Store> repo = new HashMap<>();

    @Before
    public void setup(){
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        modelMapper = new ModelMapper();

        repo.put(1L, new Store("Big Bazar", new LinkedList<>()));
        repo.put(2L, new Store("Big Market", new LinkedList<>()));
        repo.put(3L, new Store("New India", new LinkedList<>()));
        repo.put(4L, new Store("My Superstore", new LinkedList<>()));
        repo.put(5L, new Store("Big India", new LinkedList<>()));




    }

    @Test
    public void getAll() {
        List<Store> storeList = repo.values().stream().collect(Collectors.toList());
        Mockito.when(storeRepository.findByStatus(ArgumentMatchers.any())).thenReturn(storeList);
        List<StoreDto> storeDtoList = repo.values().stream().map(s->modelMapper.map(s, StoreDto.class)).collect(Collectors.toList());
        assertEquals(storeService.getAll().size(), storeDtoList.size());

    }

    @Test
    public void getStoreByIdFound() {

        Mockito.when(storeRepository.findByIdAndStatus(1L, Status.ACTIVE)).thenReturn(Optional.of(repo.get(1L)));
        assertEquals(storeService.getStoreById(1L).getName(), modelMapper.map(repo.get(1L), StoreDto.class).getName());
        verify(storeRepository, times(1)).findByIdAndStatus(1l, Status.ACTIVE);
        verifyNoMoreInteractions(storeRepository);
    }

    @Test
    public void getStoreByIdNotFoundException(){
        Mockito.when(storeRepository.findByIdAndStatus(1l, Status.ACTIVE))
                .thenThrow(new ResourceNotFoundException("NoResource"));
        String actual = Assertions.assertThrows(ResourceNotFoundException.class, ()->{
            storeService.getStoreById(1l);
        }).getMessage();
        String expected = "NoResource";
        assertEquals(expected, actual);
        verify(storeRepository, times(1)).findByIdAndStatus(1l, Status.ACTIVE);
        verifyNoMoreInteractions(storeRepository);

    }

    @Test
    public void addSuccessful() {
        StoreDto storeDto= new StoreDto("New Store");
        Store store = modelMapper.map(storeDto, Store.class);
        Mockito.when(storeRepository.save(ArgumentMatchers.any(Store.class))).thenReturn(store);
        Mockito.when(storeRepository.findByNameAndStatus(ArgumentMatchers.anyString(), ArgumentMatchers.any())).thenReturn(Optional.empty());

        repo.put(8L, modelMapper.map(storeDto, Store.class));
        assertEquals(storeService.add(storeDto).getName(), repo.get(8L).getName());
        verify(storeRepository, times(2)).findByNameAndStatus(ArgumentMatchers.anyString(), ArgumentMatchers.any());
        verify(storeRepository, times(1)).save(ArgumentMatchers.any(Store.class));
        verifyNoMoreInteractions(storeRepository);
    }

    @Test
    public void addResourceExistExceptionTest(){
        StoreDto storeDto= new StoreDto("New Store");
        Mockito.when(storeRepository.findByNameAndStatus(ArgumentMatchers.anyString(), ArgumentMatchers.any()))
                .thenThrow(new ResourceAlreadyExistsException("ResourceExist"));
        String actual = Assertions.assertThrows(ResourceAlreadyExistsException.class, ()->{
            storeService.add(storeDto);
        }).getMessage();
        String expected = "ResourceExist";
        assertEquals(expected, actual);
        verify(storeRepository, times(1)).findByNameAndStatus(ArgumentMatchers.anyString(), ArgumentMatchers.any());
        verifyNoMoreInteractions(storeRepository);
    }

    @Test
    public void updateSuccessful() {
        StoreDto storeDto= new StoreDto("New Store");
        Store store = modelMapper.map(storeDto, Store.class);
        Mockito.when(storeRepository.findByIdAndStatus(ArgumentMatchers.anyLong(), ArgumentMatchers.any())).thenReturn(Optional.of(store));
        Mockito.when(storeRepository.save(ArgumentMatchers.any(Store.class))).thenReturn(store);
        repo.put(1L, modelMapper.map(storeDto, Store.class));
        assertEquals(storeService.update(1L, storeDto).getName(), repo.get(1L).getName());
        verify(storeRepository, times(1)).findByIdAndStatus(ArgumentMatchers.anyLong(), ArgumentMatchers.any());
        verify(storeRepository, times(1)).save(ArgumentMatchers.any(Store.class));
        verifyNoMoreInteractions(storeRepository);
    }

    @Test
    public void updateThrowsResourceNotFoundException() {

        Mockito.when(storeRepository.findByIdAndStatus(ArgumentMatchers.anyLong(), ArgumentMatchers.any()))
                .thenThrow(new ResourceNotFoundException("ResourceNotFound"));

        String actual = Assertions.assertThrows(ResourceNotFoundException.class, ()->{
            storeService.update(ArgumentMatchers.anyLong(), ArgumentMatchers.any(StoreDto.class));
        }).getMessage();
        String expected = "ResourceNotFound";
        assertEquals(expected, actual);
        verify(storeRepository, times(1)).findByIdAndStatus(ArgumentMatchers.anyLong(), ArgumentMatchers.any());

        verifyNoMoreInteractions(storeRepository);
    }

    @Test
    public void deleteSuccessful() {
        StoreDto storeDto= new StoreDto("New Store");
        Store store = modelMapper.map(storeDto, Store.class);
        Mockito.when(storeRepository.findByIdAndStatus(ArgumentMatchers.anyLong(), ArgumentMatchers.any())).thenReturn(Optional.of(store));
        Mockito.when(storeRepository.save(ArgumentMatchers.any(Store.class))).thenReturn(store);
        store.setStatus(Status.INACTIVE);
        assertEquals(storeService.delete(1L).getName(), storeDto.getName());
        verify(storeRepository, times(1)).save(ArgumentMatchers.any(Store.class));
        verify(storeRepository, times(1)).findByIdAndStatus(ArgumentMatchers.anyLong(), ArgumentMatchers.any());
        verifyNoMoreInteractions(storeRepository);
    }



}