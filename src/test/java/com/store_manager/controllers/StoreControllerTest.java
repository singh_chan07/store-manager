package com.store_manager.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.store_manager.dtos.StoreDto;
import com.store_manager.exceptions.ResourceAlreadyExistsException;
import com.store_manager.exceptions.ResourceNotFoundException;
import com.store_manager.services.StoreService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.hamcrest.Matchers.*;
import static org.junit.matchers.JUnitMatchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;

import static org.junit.Assert.*;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc

public class StoreControllerTest {

    @MockBean
    StoreService storeService;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    Map<Long, StoreDto> repo = new HashMap<>();

    @Before
    public void setup(){
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();

        repo.put(1L, new StoreDto("Big Bazar"));
        repo.put(2L, new StoreDto("Big India"));
        repo.put(3L, new StoreDto("Indian Store"));
        repo.put(4L, new StoreDto("Big Mart"));
        repo.put(5L, new StoreDto("India Mart"));
        repo.put(6L, new StoreDto("Second Market"));


    }



    @Test
    public void testApplicationContextBean(){
        ServletContext servletContext = context.getServletContext();
        assertNotNull(servletContext);
    }





    @Test
    @WithMockUser("user")
    public void addStoreSuccessfulStatus201() throws Exception {
        StoreDto storeDto = new StoreDto("India Mart");
        Mockito.when(storeService.add(any())).thenReturn(storeDto);

        mockMvc.perform(post("/store-manager/stores")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsBytes(storeDto)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.name", is("India Mart")));

        ArgumentCaptor<StoreDto> dtoCaptor = ArgumentCaptor.forClass(StoreDto.class);
        verify(storeService, times(1)).add(dtoCaptor.capture());
        verifyNoMoreInteractions(storeService);
        StoreDto dtoArgument = dtoCaptor.getValue();
        assertNotNull(dtoArgument.getName());

    }

    @Test
    @WithMockUser("user")
    public void addStoreAlreadyExistStatus409() throws Exception {
        StoreDto storeDto = new StoreDto("India Mart");
        Mockito.when(storeService.add(any())).thenThrow(new ResourceAlreadyExistsException(""));

        mockMvc.perform(post("/store-manager/stores")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsBytes(storeDto)))
                .andExpect(status().isConflict())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE));

        ArgumentCaptor<StoreDto> dtoCaptor = ArgumentCaptor.forClass(StoreDto.class);
        verify(storeService, times(1)).add(dtoCaptor.capture());
        verifyNoMoreInteractions(storeService);
        StoreDto dtoArgument = dtoCaptor.getValue();
        assertNotNull(dtoArgument.getName());
    }

    @Test
    @WithMockUser("user")
    public void getAllStoresFoundStatus200() throws Exception {

        StoreDto storeDto1 = new StoreDto("Big Bazar");
        storeDto1.setId(1l);
        StoreDto storeDto2 = new StoreDto("India Mart");
        storeDto2.setId(2l);

        Mockito.when(storeService.getAll()).thenReturn(Arrays.asList(storeDto1, storeDto2));
        mockMvc.perform(MockMvcRequestBuilders
                .get("/store-manager/stores")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].name", is("Big Bazar")))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].name", is("India Mart")));

        verify(storeService, times(1)).getAll();
        verifyNoMoreInteractions(storeService);
    }

    @Test
    @WithMockUser("user")
    public void getAllStoresNotFoundStatus404() throws Exception {

        Mockito.when(storeService.getAll()).thenThrow(new ResourceNotFoundException(""));
        mockMvc.perform(MockMvcRequestBuilders
                .get("/store-manager/stores")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE));


        verify(storeService, times(1)).getAll();
        verifyNoMoreInteractions(storeService);
    }

    @Test
    @WithMockUser("user")
    public void getStoreByIdFoundStatus200() throws Exception {

        StoreDto storeDto = new StoreDto("Big Bazar");
        storeDto.setId(1l);


        Mockito.when(storeService.getStoreById(ArgumentMatchers.anyLong())).thenReturn(storeDto);
        mockMvc.perform(MockMvcRequestBuilders
                .get("/store-manager/stores/1")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is("Big Bazar")));


        verify(storeService, times(1)).getStoreById(1l);
        verifyNoMoreInteractions(storeService);
    }

    @Test
    @WithMockUser("user")
    public void getStoreByIdNotFoundStatus404() throws Exception {



        Mockito.when(storeService.getStoreById(ArgumentMatchers.anyLong())).thenThrow(new ResourceNotFoundException(""));
        mockMvc.perform(MockMvcRequestBuilders
                .get("/store-manager/stores/1")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE));


        verify(storeService, times(1)).getStoreById(1l);
        verifyNoMoreInteractions(storeService);
    }

    @Test
    @WithMockUser("user")
    public void updateSuccessfulStatus201() throws Exception {
        StoreDto storeDto = new StoreDto("India Mart");
        storeDto.setId(1l);
        Mockito.when(storeService.update(ArgumentMatchers.anyLong(),ArgumentMatchers.any(StoreDto.class))).thenReturn(storeDto);

        mockMvc.perform(put("/store-manager/stores/1")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(storeDto)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is("India Mart")));

        ArgumentCaptor<StoreDto> dtoCaptor = ArgumentCaptor.forClass(StoreDto.class);
        ArgumentCaptor<Long> idCaptor = ArgumentCaptor.forClass(Long.class);

        verify(storeService, times(1)).update(idCaptor.capture(), dtoCaptor.capture());
        verifyNoMoreInteractions(storeService);

        Long idArgument = idCaptor.getValue();
        StoreDto dtoArgument = dtoCaptor.getValue();
        assertNotNull(dtoArgument.getName());
        assertNotNull(idArgument);


    }

    @Test
    @WithMockUser("user")
    public void deleteSuccessfulStatus201() throws Exception {
        StoreDto storeDto = new StoreDto("India Mart");
        storeDto.setId(1l);
        Mockito.when(storeService.delete(ArgumentMatchers.anyLong())).thenReturn(storeDto);

        mockMvc.perform(MockMvcRequestBuilders
                .delete("/store-manager/stores/1")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is("India Mart")));

        ArgumentCaptor<Long> idCaptor = ArgumentCaptor.forClass(Long.class);

        verify(storeService, times(1)).delete(idCaptor.capture());
        verifyNoMoreInteractions(storeService);

        Long idArgument = idCaptor.getValue();
        assertNotNull(idArgument);
    }
}