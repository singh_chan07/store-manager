package com.store_manager.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.store_manager.dtos.ItemDto;
import com.store_manager.clients.ItemClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;


@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class ItemControllerTest {

    @MockBean
    ItemClient itemClient;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;



    @Before
    public void setup(){
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
    }


    @Test
    public void getItems() throws Exception {

        ItemDto itemDto1 = new ItemDto("Item1", 1);
        itemDto1.setId(1l);

        ItemDto itemDto2 = new ItemDto("Item2", 1);
        itemDto2.setId(2l);

        Mockito.when(itemClient.getItems(ArgumentMatchers.anyLong())).thenReturn(Arrays.asList(itemDto1, itemDto2));
        mockMvc.perform(MockMvcRequestBuilders
                .get("/store-manager/stores/1/brands/1/items")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].name", is("Item1")))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].name", is("Item2")));

        verify(itemClient, times(1)).getItems(1);
        verifyNoMoreInteractions(itemClient);
    }

    @Test
    public void addItem() throws Exception {


        ItemDto itemDto = new ItemDto("Item1", 1);
        itemDto.setId(1l);

        Mockito.when(itemClient.add(ArgumentMatchers.any())).thenReturn(itemDto);
        mockMvc.perform(MockMvcRequestBuilders
                .post("/store-manager/stores/1/brands")
                .content(objectMapper.writeValueAsString(itemDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is("Item1")))
                .andExpect(jsonPath("$.brandId", is(1)));



        ArgumentCaptor<ItemDto> dtoCaptor = ArgumentCaptor.forClass(ItemDto.class);
        ArgumentCaptor<Long> idCaptor = ArgumentCaptor.forClass(Long.class);

        verify(itemClient, times(1)).add(dtoCaptor.capture());
        verifyNoMoreInteractions(itemClient);
        Long idArgument = idCaptor.getValue();
        assertNotNull(idArgument);
        ItemDto dtoArgument = dtoCaptor.getValue();
        assertNotNull(dtoArgument.getName());
    }

    @Test
    public void deleteItem() throws Exception {

        ItemDto itemDto = new ItemDto("Item1", 1);
        itemDto.setId(1l);

        Mockito.when(itemClient.delete(ArgumentMatchers.anyLong())).thenReturn("Deleted");
        mockMvc.perform(MockMvcRequestBuilders
                .post("/store-manager/stores/1/brands")
                .content(objectMapper.writeValueAsString(itemDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated());
    }
}