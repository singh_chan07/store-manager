package com.store_manager.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.store_manager.dtos.BrandDto;
import com.store_manager.dtos.StoreDto;
import com.store_manager.entities.Brand;
import com.store_manager.exceptions.ResourceAlreadyExistsException;
import com.store_manager.exceptions.ResourceNotFoundException;
import com.store_manager.services.BrandService;
import com.store_manager.services.StoreService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class BrandControllerTest {

    @MockBean
    BrandService brandService;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    Map<Long, StoreDto> repo = new HashMap<>();

    @Before
    public void setup(){
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
    }



    @Test
    public void testApplicationContextBean(){
        ServletContext servletContext = context.getServletContext();
        assertNotNull(servletContext);
    }





    @Test
    @WithMockUser("user")
    public void addBrandSuccessfulStatus201() throws Exception {
        BrandDto brandDto = new BrandDto("Brand One", "BO");
        brandDto.setId(1l);

        Mockito.when(brandService.add(ArgumentMatchers.anyLong(), ArgumentMatchers.any(BrandDto.class))).thenReturn(brandDto);
        mockMvc.perform(MockMvcRequestBuilders
                .post("/store-manager/stores/1/brands")
                .content(objectMapper.writeValueAsString(brandDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is("Brand One")));



        ArgumentCaptor<BrandDto> dtoCaptor = ArgumentCaptor.forClass(BrandDto.class);
        ArgumentCaptor<Long> idCaptor = ArgumentCaptor.forClass(Long.class);

        verify(brandService, times(1)).add(idCaptor.capture(), dtoCaptor.capture());
        verifyNoMoreInteractions(brandService);
        Long idArgument = idCaptor.getValue();
        assertNotNull(idArgument);
        BrandDto dtoArgument = dtoCaptor.getValue();
        assertNotNull(dtoArgument.getName());

    }

    @Test
    @WithMockUser("user")
    public void addBrandAlreadyExistStatus409() throws Exception {

        BrandDto brandDto = new BrandDto("Brand One", "BO");
        brandDto.setId(1l);
        Mockito.when(brandService.add(ArgumentMatchers.anyLong(), ArgumentMatchers.any(BrandDto.class)))
                .thenThrow(new ResourceAlreadyExistsException(" "));
        mockMvc.perform(MockMvcRequestBuilders
                .post("/store-manager/stores/1/brands")
                .content(objectMapper.writeValueAsString(brandDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isConflict());

        ArgumentCaptor<BrandDto> dtoCaptor = ArgumentCaptor.forClass(BrandDto.class);
        ArgumentCaptor<Long> idCaptor = ArgumentCaptor.forClass(Long.class);

        verify(brandService, times(1)).add(idCaptor.capture(), dtoCaptor.capture());
        verifyNoMoreInteractions(brandService);
        Long idArgument = idCaptor.getValue();
        assertNotNull(idArgument);
        BrandDto dtoArgument = dtoCaptor.getValue();
        assertNotNull(dtoArgument.getName());
    }

    @Test
    @WithMockUser("user")
    public void getAllBrandsFoundStatus200() throws Exception {

        BrandDto brandDto1 = new BrandDto("Brand One", "BO");
        brandDto1.setId(1l);
        BrandDto brandDto2 = new BrandDto("Brand Two", "BT");
        brandDto2.setId(2l);

        Mockito.when(brandService.getAllByStore(ArgumentMatchers.anyLong())).thenReturn(Arrays.asList(brandDto1, brandDto2));
        mockMvc.perform(MockMvcRequestBuilders
                .get("/store-manager/stores/1/brands")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].name", is("Brand One")))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].name", is("Brand Two")));

        verify(brandService, times(1)).getAllByStore(1);
        verifyNoMoreInteractions(brandService);
    }

    @Test
    @WithMockUser("user")
    public void getAllBrandsNotFoundStatus404() throws Exception {

        Mockito.when(brandService.getAllByStore(ArgumentMatchers.anyLong())).thenThrow(new ResourceNotFoundException(" "));
        mockMvc.perform(MockMvcRequestBuilders
                .get("/store-manager/stores/1/brands")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound());
        verify(brandService, times(1)).getAllByStore(1);
        verifyNoMoreInteractions(brandService);
    }

    @Test
    @WithMockUser("user")
    public void getBrandByIdFoundStatus200() throws Exception {

        BrandDto brandDto = new BrandDto("Brand One", "BO");
        brandDto.setId(1l);

        Mockito.when(brandService.get(ArgumentMatchers.anyLong())).thenReturn(brandDto);
        mockMvc.perform(MockMvcRequestBuilders
                .get("/store-manager/stores/1/brands/1")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is("Brand One")));

        verify(brandService, times(1)).get(1);
        verifyNoMoreInteractions(brandService);
    }

    @Test
    @WithMockUser("user")
    public void getBrandByIdNotFoundStatus404() throws Exception {

        Mockito.when(brandService.get(ArgumentMatchers.anyLong())).thenThrow(new ResourceNotFoundException(""));
        mockMvc.perform(MockMvcRequestBuilders
                .get("/store-manager/stores/1/brands/1")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound());
        verify(brandService, times(1)).get(1);
        verifyNoMoreInteractions(brandService);
    }

    @Test
    @WithMockUser("user")
    public void updateSuccessfulStatus201() throws Exception {
        BrandDto brandDto = new BrandDto("Brand One", "BO");
        brandDto.setId(1l);

        Mockito.when(brandService.update(ArgumentMatchers.anyLong(), ArgumentMatchers.any(BrandDto.class))).thenReturn(brandDto);
        mockMvc.perform(MockMvcRequestBuilders
                .put("/store-manager/stores/1/brands/1")
                .content(objectMapper.writeValueAsString(brandDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is("Brand One")));



        ArgumentCaptor<BrandDto> dtoCaptor = ArgumentCaptor.forClass(BrandDto.class);
        ArgumentCaptor<Long> idCaptor = ArgumentCaptor.forClass(Long.class);
        verify(brandService, times(1)).update(idCaptor.capture(), dtoCaptor.capture());
        verifyNoMoreInteractions(brandService);
        Long idArgument = idCaptor.getValue();
        assertNotNull(idArgument);
        BrandDto dtoArgument = dtoCaptor.getValue();
        assertNotNull(dtoArgument.getName());


    }

    @Test
    @WithMockUser("user")
    public void deleteSuccessfulStatus201() throws Exception {
        BrandDto brandDto = new BrandDto("Brand One", "BO");
        brandDto.setId(1l);

        Mockito.when(brandService.delete(ArgumentMatchers.anyLong())).thenReturn(brandDto);
        mockMvc.perform(MockMvcRequestBuilders
                .delete("/store-manager/stores/1/brands/1")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is("Brand One")));

        ArgumentCaptor<Long> idCaptor = ArgumentCaptor.forClass(Long.class);
        verify(brandService, times(1)).delete(idCaptor.capture());
        verifyNoMoreInteractions(brandService);
        Long idArgument = idCaptor.getValue();
        assertNotNull(idArgument);

    }
}