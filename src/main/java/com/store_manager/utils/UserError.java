package com.store_manager.utils;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "user")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserError {
    String notFound;
    String alreadyExist;
}
