package com.store_manager.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public abstract class ErrorObject {

    private Date timeStamp;

    private int status;

    private String path;
}
