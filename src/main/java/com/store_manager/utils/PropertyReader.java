package com.store_manager.utils;


import lombok.*;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PropertyReader {
    Name name;
    Found found;
    NotFound notFound;

    @Data
    public static class Name{
        String notNull;
        String notBlank;
    }

    @Data
    public static class Found{
        String byId;
        String byName;
    }

    @Data
    public static class NotFound{
        String none;
        String byId;
        String byName;
    }
}
