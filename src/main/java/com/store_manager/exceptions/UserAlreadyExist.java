package com.store_manager.exceptions;

public class UserAlreadyExist extends BaseException{
    public UserAlreadyExist(String message) {
        super(message);
    }
}
