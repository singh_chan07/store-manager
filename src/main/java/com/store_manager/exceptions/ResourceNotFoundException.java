package com.store_manager.exceptions;

public class ResourceNotFoundException extends BaseException{

    public ResourceNotFoundException(String message) {
        super(message);
    }
}
