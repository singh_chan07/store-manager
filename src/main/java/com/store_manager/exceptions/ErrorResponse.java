package com.store_manager.exceptions;

import com.store_manager.utils.ErrorObject;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.util.Date;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ErrorResponse extends ErrorObject {

    private String message;

    public ErrorResponse(Date date, HttpStatus status, String path,  String message){
        super(date, status.value(), path);
        this.message = message;
    }

}
