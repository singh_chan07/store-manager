package com.store_manager.exceptions;

public class ResourceAlreadyExistsException extends BaseException{

    public ResourceAlreadyExistsException(String message) {
        super(message);
    }
}
