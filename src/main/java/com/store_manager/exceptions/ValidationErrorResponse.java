package com.store_manager.exceptions;

import com.store_manager.utils.ErrorObject;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.util.Date;
import java.util.Map;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ValidationErrorResponse extends ErrorObject {

    Map<String, String> errors;

    public ValidationErrorResponse(Date date, HttpStatus status, String path, Map<String, String> errors){
        super(date, status.value(), path);
        this.errors = errors;
    }
}
