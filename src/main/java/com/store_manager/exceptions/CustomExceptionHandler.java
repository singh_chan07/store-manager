package com.store_manager.exceptions;

import io.jsonwebtoken.ExpiredJwtException;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {



    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getFieldErrors().stream()
                .forEach(e-> errors.put(e.getField(), e.getDefaultMessage()));
        ValidationErrorResponse errorResponse = new ValidationErrorResponse(
                new Date(),
                HttpStatus.BAD_REQUEST,
                request.getDescription(false),
                errors
        );

        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }



    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(
            NoHandlerFoundException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return new ResponseEntity<>(new ErrorResponse(
                new Date(), HttpStatus.BAD_REQUEST, request.getDescription(false),
                ex.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMissingPathVariable(
            MissingPathVariableException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {

        ErrorResponse errorResponse = new ErrorResponse(
                new Date(), HttpStatus.BAD_REQUEST,
                request.getDescription(false),
                ex.getVariableName()+" : "+ ex.getMessage());
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(
            HttpRequestMethodNotSupportedException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {
        ErrorResponse errorResponse = new ErrorResponse(new Date(),
                HttpStatus.BAD_REQUEST,  request.getDescription(false), ex.getMessage());
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<Object> handleResourceNotFoundException(
            ResourceNotFoundException ex, WebRequest webRequest){
        ErrorResponse errorResponse = new ErrorResponse(
                new Date(), HttpStatus.NOT_FOUND,webRequest.getDescription(false),
                ex.getMessage());
        return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
    }


    @ExceptionHandler(UserNotFound.class)
    public ResponseEntity<Object> handleUserNotFoundException(
            UserNotFound ex, WebRequest webRequest){
        ErrorResponse errorResponse = new ErrorResponse(
                new Date(), HttpStatus.NOT_FOUND,webRequest.getDescription(false),
                ex.getMessage());
        return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ResourceAlreadyExistsException.class)
    public ResponseEntity<Object> handleResourceExistsException(
            ResourceAlreadyExistsException ex, WebRequest webRequest){
        ErrorResponse errorResponse = new ErrorResponse(new Date(),
                HttpStatus.CONFLICT,webRequest.getDescription(false),
                ex.getMessage());
        return new ResponseEntity<>(errorResponse, HttpStatus.CONFLICT);
    }
    @ExceptionHandler(UserAlreadyExist.class)
    public ResponseEntity<Object> handleUserExistsException(
            UserAlreadyExist ex, WebRequest webRequest){
        ErrorResponse errorResponse = new ErrorResponse(new Date(),
                HttpStatus.CONFLICT,webRequest.getDescription(false),
                ex.getMessage());
        return new ResponseEntity<>(errorResponse, HttpStatus.CONFLICT);
    }



    @ExceptionHandler(BaseException.class)
    public ResponseEntity<Object> handleBaseException(BaseException ex, WebRequest webRequest){
        ErrorResponse errorResponse = new ErrorResponse(new Date(),
                HttpStatus.INTERNAL_SERVER_ERROR,webRequest.getDescription(false),
                ex.getMessage());
        return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }


}
