package com.store_manager.exceptions;

public class UserNotFound extends BaseException{
    public UserNotFound(String message) {
        super(message);
    }
}
