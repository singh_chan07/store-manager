package com.store_manager.dtos;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StoreDto extends BaseDto{



    @NotNull(message = "{store.name.notNull}")
    @NotBlank(message = "{store.name.notBlank}")
    private String name;


}
