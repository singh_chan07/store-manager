package com.store_manager.dtos;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserDto extends BaseDto{

    @NotNull(message = "{user.notNull}")
    @NotBlank(message = "{user.notBlank}")
    private String userName;

    @NotNull(message = "{password.notNull}")
    @NotBlank(message = "{password.notBlank}")
    private String password;
}
