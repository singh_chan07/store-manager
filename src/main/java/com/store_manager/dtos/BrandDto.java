package com.store_manager.dtos;


import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BrandDto extends BaseDto{


    @NotNull(message = "{brand.name.notNull}")
    @NotBlank(message = "{brand.name.notBlank}")
    private String name;

    @NotNull(message = "{brand.initials.notNull}")
    @NotBlank(message = "{brand.initials.notBlank}")
    @Size(min = 2, max = 2, message = "{brand.initials.size}")
    private String initials;
}
