package com.store_manager.dtos;


import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ItemDto extends BaseDto{

    @NotBlank(message = "{item.notBlank}")
    @NotNull(message = "{item.notNull}")
    private String name;

    @NotBlank(message = "{brandId.notBlank}")
    @NotNull(message = "{brandId.notNull}")
    private long brandId;
}
