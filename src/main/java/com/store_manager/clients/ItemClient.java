package com.store_manager.clients;

import com.store_manager.dtos.ItemDto;
import org.springframework.web.bind.annotation.*;

import java.util.List;


public interface ItemClient {

    @GetMapping("/{brandId}")
    public List<ItemDto> getItems(@PathVariable long brandId);

    @PostMapping
    public ItemDto add(@RequestBody ItemDto itemDto);

    @DeleteMapping("/{itemId}")
    public String delete(@PathVariable long itemId);
}
