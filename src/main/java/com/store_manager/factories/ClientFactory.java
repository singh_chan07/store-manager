package com.store_manager.factories;

public interface ClientFactory {

    <T> T createFeignClient(Class<T> type, String url);
}
