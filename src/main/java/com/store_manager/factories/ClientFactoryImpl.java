package com.store_manager.factories;

import feign.Feign;
import org.springframework.cloud.openfeign.support.SpringMvcContract;
import org.springframework.stereotype.Component;

@Component
public class ClientFactoryImpl implements ClientFactory{

    @Override
    public <T> T createFeignClient(Class<T> type, String url) {
        return Feign.builder()
                .contract(new SpringMvcContract())
                .target(type, url);
    }

}
