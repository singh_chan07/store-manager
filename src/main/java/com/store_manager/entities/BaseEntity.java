package com.store_manager.entities;

import com.store_manager.utils.Status;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@MappedSuperclass
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BaseEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String createdBy;

    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    private String updatedBy;

    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;


    private Status status = Status.ACTIVE;

    @PrePersist
    public void setCreationDate(){
        this.createdOn = new Date();

    }

    @PreUpdate
    public void setChangeDate(){
        this.updatedOn = new Date();

    }



}
