package com.store_manager.controllers;


import com.store_manager.dtos.StoreDto;
import com.store_manager.services.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;


@RestController
@RequestMapping(
        value = "/store-manager/stores",
        produces = MediaType.APPLICATION_JSON_VALUE,
        consumes = MediaType.APPLICATION_JSON_VALUE
)
public class StoreController {

    @Autowired
    StoreService storeService;



    @PostMapping
    public ResponseEntity<StoreDto> add(@RequestBody @Valid StoreDto storeDto){

        storeDto = storeService.add(storeDto);
        return new ResponseEntity<>(storeDto, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<StoreDto>> getAll(){
        List<StoreDto> storeDtoList = storeService.getAll();
        return new ResponseEntity<>(storeDtoList, HttpStatus.OK);
    }

    @GetMapping("/{storeId}")
    public StoreDto get(@PathVariable long storeId)  {
        return storeService.getStoreById(storeId);
    }

    @PutMapping("/{storeId}")
    public ResponseEntity<StoreDto> update(@PathVariable long storeId, @RequestBody @Valid StoreDto storeDto){
        storeDto = storeService.update(storeId, storeDto);
        return new ResponseEntity<>(storeDto, HttpStatus.OK);
    }

    @DeleteMapping("/{storeId}")
    public ResponseEntity<StoreDto> delete(@PathVariable @NotNull long storeId){

        return new ResponseEntity<>(storeService.delete(storeId), HttpStatus.OK);
    }




}
