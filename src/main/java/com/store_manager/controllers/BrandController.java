package com.store_manager.controllers;


import com.store_manager.dtos.BrandDto;
import com.store_manager.entities.Brand;
import com.store_manager.services.BrandService;
import com.store_manager.services.BrandServiceImpl;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(
        value = "store-manager/stores/{storeId}/brands",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE
)
public class BrandController {

    @Autowired
    BrandService brandService;



    @GetMapping
    public ResponseEntity<List<BrandDto>> getAll(@PathVariable long storeId){
        List<BrandDto> brandDtoList = brandService.getAllByStore(storeId);
        return new ResponseEntity<>(brandDtoList, HttpStatus.OK);
    }

    @GetMapping("/{brandId}")
    public ResponseEntity<BrandDto> get(@PathVariable long brandId){
        BrandDto brandDto = brandService.get(brandId);
        return new ResponseEntity<>(brandDto, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<BrandDto> add(@PathVariable long storeId, @RequestBody @Valid BrandDto brandDto){

        brandDto =  brandService.add(storeId, brandDto);
        return new ResponseEntity<>(brandDto, HttpStatus.CREATED);
    }

    @PutMapping("/{brandId}")
    public ResponseEntity<BrandDto> update(@PathVariable long brandId, @RequestBody BrandDto brandDto){
        brandDto = brandService.update(brandId, brandDto);
        return new ResponseEntity<>(brandDto, HttpStatus.OK);
    }

    @DeleteMapping("/{brandId}")
    public ResponseEntity<BrandDto> delete(@PathVariable long brandId){

        return new ResponseEntity<>(brandService.delete(brandId), HttpStatus.OK);
    }
}
