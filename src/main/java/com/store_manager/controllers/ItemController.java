package com.store_manager.controllers;


import com.store_manager.dtos.ItemDto;
import com.store_manager.factories.ClientFactory;
import com.store_manager.clients.ItemClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(
        value = "/store-manager/stores/{storeId}/brands/{brandId}/items",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE
)
public class ItemController {


    private ClientFactory factory;

    public ItemController(ClientFactory factory) {
        this.factory = factory;
        itemClient = factory.createFeignClient(ItemClient.class, "${item.client.url}");
    }

    private ItemClient itemClient;

    @GetMapping
    public ResponseEntity<List<ItemDto>> getItems(@PathVariable long brandId){
        return new ResponseEntity<>(itemClient.getItems(brandId), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<ItemDto> addItem(@RequestBody ItemDto itemDto){
        return new ResponseEntity<>(itemClient.add(itemDto), HttpStatus.CREATED);
    }

    @DeleteMapping("/{itemId}")
    public ResponseEntity<Object> deleteItem(@PathVariable long itemId){
        return new ResponseEntity<>(itemClient.delete(itemId), HttpStatus.OK);
    }

}
