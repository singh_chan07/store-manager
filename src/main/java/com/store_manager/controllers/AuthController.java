package com.store_manager.controllers;


import com.store_manager.dtos.UserDto;
import com.store_manager.dtos.TokenDto;
import com.store_manager.entities.User;
import com.store_manager.services.AuthService;
import com.store_manager.services.UserService;
import com.store_manager.utils.JwtTokenUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/store-manager/auth")
@Slf4j
public class AuthController {



    @Autowired
    JwtTokenUtil jwtTokenUtil;

    @Autowired
    AuthService authService;

    @Autowired
    UserService userService;

    @PostMapping("/sign-up")
    public ResponseEntity<Object> signUp(@RequestBody @Valid UserDto userDto){

        authService.signup(userDto);
        return new ResponseEntity<>("User registered successfully", HttpStatus.CREATED);

    }

    @PostMapping("/sign-in")
    public ResponseEntity<Object> logIn(@RequestBody @Valid UserDto userDto){
        TokenDto tokenDto = authService.login(userDto);

        return ResponseEntity.ok()
                .header(
                        HttpHeaders.AUTHORIZATION,
                        tokenDto.getJwt()
                )
                .body("User logged in successfully");
    }

    @GetMapping("/sign-out")
    public ResponseEntity<Object> logOut(@RequestHeader("Authorization") String authorization){
        String jwt;
        String username = null;
        if(authorization!=null && authorization.startsWith("Bearer ")){
            jwt = authorization.substring(7).trim();

            username = jwtTokenUtil.extractUsername(jwt);
        }
        if (username!=null){
            User user = userService.getUser(username);
            authService.logOut(user.getUsername());
        }

        return ResponseEntity.ok()
                .body("User logged out successfully");
    }
}
