package com.store_manager.repositories;

import com.store_manager.entities.BaseEntity;
import com.store_manager.utils.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;
import java.util.Optional;

@NoRepositoryBean
public interface BaseRepository<T, ID> extends JpaRepository<T, ID> {

    Optional<T> findByIdAndStatus(long id, Status status);

    List<T> findByStatus(Status status);

}
