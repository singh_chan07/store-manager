package com.store_manager.repositories;


import com.store_manager.entities.Brand;
import com.store_manager.utils.Status;

import java.util.Optional;

public interface BrandRepository extends BaseRepository<Brand, Long>{

    Optional<Brand> findByInitialsAndStatus(String initials, Status status);
    Optional<Brand> findByNameAndStatus(String name, Status status);

}
