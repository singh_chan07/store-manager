package com.store_manager.repositories;


import com.store_manager.entities.Store;
import com.store_manager.utils.Status;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StoreRepository extends BaseRepository<Store, Long> {

    Optional<Store> findByNameAndStatus(String name, Status status);
}
