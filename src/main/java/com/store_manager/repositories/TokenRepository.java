package com.store_manager.repositories;

import com.store_manager.entities.Token;
import com.store_manager.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TokenRepository extends BaseRepository<Token, Long> {

    Optional<Token> findByUserAndExpiredFalse(User user);
}
