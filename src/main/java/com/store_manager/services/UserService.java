package com.store_manager.services;

import com.store_manager.entities.Role;
import com.store_manager.entities.User;

import java.util.Set;

public interface UserService {

    User add(User user);
    User getUser(String userName);
    void deleteUser(String userName);
    Boolean userExist(String username);
    Set<Role> getRoles(String userName);
}
