package com.store_manager.services;

import com.store_manager.dtos.UserDto;
import com.store_manager.dtos.TokenDto;

public interface AuthService {

    TokenDto login(UserDto userDto);

    void signup(UserDto userDto);

    void logOut(String username);
}
