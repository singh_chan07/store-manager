package com.store_manager.services;


import com.store_manager.dtos.BaseDto;
import com.store_manager.dtos.StoreDto;
import com.store_manager.entities.BaseEntity;
import com.store_manager.utils.PropertyReader;
import com.store_manager.utils.Status;
import com.store_manager.entities.Store;
import com.store_manager.exceptions.ResourceAlreadyExistsException;
import com.store_manager.exceptions.ResourceNotFoundException;
import com.store_manager.repositories.StoreRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service

public class StoreServiceImpl extends BaseService implements StoreService{



    @Autowired
    private StoreRepository storeRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private PropertyReader storeMessage;



    public List<StoreDto> getAll(){

        List<Store> stores = storeRepository.findByStatus(Status.ACTIVE);

        return stores.stream().map(s-> convertToDto(s)).collect(Collectors.toList());
    }

    public StoreDto getStoreById(long storeId){

        return convertToDto(storeRepository.findByIdAndStatus(storeId, Status.ACTIVE)
                .orElseThrow(()->new ResourceNotFoundException(storeMessage.getNotFound().getById())));

    }

    public StoreDto add(StoreDto storeDto){
        Store store = new Store();
        if(storeRepository.findByNameAndStatus(storeDto.getName(), Status.ACTIVE).isPresent()){
            throw  new ResourceAlreadyExistsException(storeMessage.getFound().getByName());
        }else if( storeRepository.findByNameAndStatus(storeDto.getName(), Status.INACTIVE).isPresent()){
            store = storeRepository.findByNameAndStatus(store.getName(), Status.INACTIVE).get();
            store.setStatus(Status.ACTIVE);
            return convertToDto(storeRepository.save(store));
        }else{
            store = convertToEntity(storeDto);
            return convertToDto(storeRepository.save(store));
        }

    }



    public StoreDto update(long storeId, StoreDto temp){
        Store store = storeRepository.findByIdAndStatus(storeId, Status.ACTIVE)
                .or(()->storeRepository.findByIdAndStatus(storeId, Status.INACTIVE))
                .orElseThrow(()->new ResourceNotFoundException(storeMessage.getNotFound().getById()));
        store.setName(temp.getName());
        store.setStatus(Status.ACTIVE);
        return convertToDto(storeRepository.save(store));
    }

    public StoreDto delete(long storeId){
        Store store = storeRepository.findByIdAndStatus(storeId, Status.ACTIVE)
                .orElseThrow(()->new ResourceNotFoundException(storeMessage.getNotFound().getById()));
        store.setStatus(Status.INACTIVE);
        return convertToDto(storeRepository.save(store));
    }


    private StoreDto convertToDto(Store store){
        return modelMapper.map(store, StoreDto.class);
    }

    private Store convertToEntity(StoreDto storeDto){
        return modelMapper.map(storeDto, Store.class);
    }
}
