package com.store_manager.services;

import com.store_manager.entities.Role;
import com.store_manager.entities.User;
import com.store_manager.exceptions.UserAlreadyExist;
import com.store_manager.exceptions.UserNotFound;
import com.store_manager.repositories.UserRepository;
import com.store_manager.utils.Status;
import com.store_manager.utils.UserError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class UserServiceImpl implements UserService{
    @Autowired
    UserRepository userRepository;

    @Autowired
    UserError userError;

    @Override
    public User add(User user) {
        if(userRepository.findByUserName(user.getUsername()).isPresent()){
            new UserAlreadyExist(userError.getAlreadyExist());
        }
        return userRepository.save(user);
    }

    @Override
    public User getUser(String userName) {
        return userRepository.findByUserName(userName)
                .orElseThrow(()->new UserNotFound(userError.getNotFound()));
    }

    @Override
    public void deleteUser(String userName) {
        User user = userRepository.findByUserName(userName)
                .orElseThrow(()->new UserNotFound(userError.getNotFound()));
        user.setStatus(Status.INACTIVE);
        userRepository.save(user);
    }

    @Override
    public Boolean userExist(String username) {
        return userRepository.findByUserName(username).isPresent();
    }

    @Override
    public Set<Role> getRoles(String userName) {
        return getUser(userName).getRoles();
    }
}
