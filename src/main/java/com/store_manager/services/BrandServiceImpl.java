package com.store_manager.services;


import com.store_manager.dtos.BrandDto;
import com.store_manager.entities.Brand;
import com.store_manager.utils.PropertyReader;
import com.store_manager.utils.Status;
import com.store_manager.entities.Store;
import com.store_manager.exceptions.ResourceAlreadyExistsException;
import com.store_manager.exceptions.ResourceNotFoundException;
import com.store_manager.repositories.BrandRepository;
import com.store_manager.repositories.StoreRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BrandServiceImpl extends BaseService implements BrandService{
    @Autowired
    private BrandRepository brandRepository;

    @Autowired
    private StoreRepository storeRepository;


    @Autowired
    PropertyReader brandMessage;

    @Autowired
    ModelMapper modelMapper;




    public BrandDto get(long brandId){
        return convertToDto(brandRepository.findByIdAndStatus(brandId, Status.ACTIVE)
                .orElseThrow(()-> new ResourceNotFoundException(brandMessage.getNotFound().getById())));
    }

    public BrandDto update(long brandId, BrandDto update){
        Brand brand = brandRepository.findByIdAndStatus(brandId, Status.ACTIVE)
                .orElseThrow(()->new ResourceNotFoundException(brandMessage.getNotFound().getById()));
        brand.setName(update.getName());
        brand.setInitials(update.getInitials());
        return convertToDto(brandRepository.save(brand));
    }

    public BrandDto delete(long brandId){

        Brand brand = brandRepository.findByIdAndStatus(brandId, Status.ACTIVE)
                .orElseThrow(()-> new ResourceNotFoundException(brandMessage.getNotFound().getById()));
        brand.setStatus(Status.INACTIVE);
        return convertToDto(brandRepository.save(brand));

    }

    public List<BrandDto> getAllByStore(long storeId){

        Store store = storeRepository.findByIdAndStatus(storeId, Status.ACTIVE)
                .orElseThrow(() ->new ResourceNotFoundException(brandMessage.getNotFound().getById()));
        return store.getBrandList().stream().
                filter(b->b.getStatus()==Status.ACTIVE).map(b-> convertToDto(b)).collect(Collectors.toList());

    }

    public BrandDto add(long storeId, BrandDto brandDto){

        Store store = storeRepository.findById(storeId)
                .orElseThrow(() ->new ResourceNotFoundException(brandMessage.getNotFound().getById()));
        long count = store.getBrandList().stream().filter(b-> b.getName().equals(brandDto.getName())).count();
        if(count==0){
            Brand brand = convertToEntity(brandDto);
            brand.setStore(store);
            return convertToDto(brandRepository.save(brand));
        }else{

            throw  new ResourceAlreadyExistsException(brandMessage.getFound().getByName());
        }

    }

    private BrandDto convertToDto(Brand brand){
        return modelMapper.map(brand, BrandDto.class);
    }

    private Brand convertToEntity(BrandDto brandDto){
        return modelMapper.map(brandDto, Brand.class);
    }


}
