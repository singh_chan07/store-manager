package com.store_manager.services;

import com.store_manager.dtos.StoreDto;
import com.store_manager.entities.Store;

import java.util.List;

public interface StoreService {

    List<StoreDto> getAll();

    StoreDto getStoreById(long storeId);

    StoreDto add(StoreDto storeDto);

    StoreDto update(long storeId, StoreDto temp);

    StoreDto delete(long storeId);
}
