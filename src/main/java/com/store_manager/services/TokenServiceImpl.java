package com.store_manager.services;

import com.store_manager.entities.Token;
import com.store_manager.dtos.TokenDto;
import com.store_manager.entities.User;
import com.store_manager.repositories.TokenRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@Service
public class TokenServiceImpl implements TokenService {

    @Autowired
    private UserService userService;

    @Autowired
    private TokenRepository tokenRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public TokenDto get(String username) {
        User user = userService.getUser(username);
        Optional<Token> token = tokenRepository.findByUserAndExpiredFalse(user);
        if(token.isPresent()){
            return convertToDto(token.get());
        }else {
            return null;
        }
    }

    @Override
    public TokenDto save(TokenDto tokenDto, String username) {
        User user = userService.getUser(username);
        Token token = modelMapper.map(tokenDto, Token.class);
        token.setUser(user);
        return convertToDto(tokenRepository.save(token));

    }

    @Override
    public void setExpiredTrue(String username) {
        User user = userService.getUser(username);
        Token token = tokenRepository.findByUserAndExpiredFalse(user).get();
        token.setExpired(true);
        tokenRepository.save(token);

    }

    @Override
    public Boolean isTokenExpired(String username) {
        User user = userService.getUser(username);
        Optional<Token> token = tokenRepository.findByUserAndExpiredFalse(user);
        if (token.isPresent()){
            return token.get().isExpired();
        }else{
            return true;
        }

    }

    private TokenDto convertToDto(Token token){
        return modelMapper.map(token, TokenDto.class);
    }

    private Token convertToEntity(TokenDto tokenDto){
        return modelMapper.map(tokenDto, Token.class);
    }
}
