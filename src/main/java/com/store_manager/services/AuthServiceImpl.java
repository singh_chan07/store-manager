package com.store_manager.services;

import com.store_manager.dtos.UserDto;
import com.store_manager.dtos.TokenDto;
import com.store_manager.entities.Role;
import com.store_manager.entities.User;
import com.store_manager.exceptions.UserAlreadyExist;
import com.store_manager.exceptions.UserNotFound;
import com.store_manager.utils.JwtTokenUtil;
import com.store_manager.utils.UserError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class AuthServiceImpl implements AuthService{

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtTokenUtil jwtTokenUtil;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    UserService userService;

    @Autowired
    TokenService tokenService;

    @Autowired
    UserError userError;

    public TokenDto login(UserDto userDto){

        Authentication authentication  =  authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(userDto.getUserName(), userDto.getPassword())
        );
        User user = (User) authentication.getPrincipal();
        if (user==null){
            throw new UserNotFound(userError.getNotFound());
        }
        TokenDto tokenDto = tokenService.get(user.getUsername());
        if(tokenDto!=null){
            tokenService.setExpiredTrue(user.getUsername());
        }

        tokenDto = tokenService.save(
                new TokenDto(jwtTokenUtil.generateToken(user)), user.getUsername()
        );
        return tokenDto;

    }

    public void signup(UserDto userDto){

        if(userService.userExist(userDto.getUserName())){
            throw new UserAlreadyExist(userError.getAlreadyExist());
        }
        User user = new User();
        Role userRole = new Role();
        userRole.setRole("Admin");
        user.setUserName(userDto.getUserName());
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        user.setRoles(Set.of(userRole));
        userService.add(user);

    }

    @Override
    public void logOut(String username) {

        TokenDto tokenDto = tokenService.get(username);
        if(tokenDto!=null){
            tokenService.setExpiredTrue(username);
        }

    }
}
