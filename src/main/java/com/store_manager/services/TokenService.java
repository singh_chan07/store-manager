package com.store_manager.services;

import com.store_manager.dtos.TokenDto;
import com.store_manager.entities.Token;

public interface TokenService {

    TokenDto get(String username);

    TokenDto save(TokenDto tokenDto, String username);

    void setExpiredTrue(String username);

    Boolean isTokenExpired(String username);
}
