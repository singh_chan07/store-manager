package com.store_manager.services;

import com.store_manager.dtos.BaseDto;
import com.store_manager.dtos.BrandDto;
import com.store_manager.entities.Brand;

import java.util.List;

public interface BrandService {

    BrandDto get(long brandId);
    BrandDto update(long brandId, BrandDto update);
    BrandDto delete(long brandId);
    List<BrandDto> getAllByStore(long storeId);
    BrandDto add(long storeId, BrandDto brand);
}
